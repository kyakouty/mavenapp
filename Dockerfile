FROM openjdk:17
LABEL maintainer="Kamal Yakouty"
ADD target/maven-third-app-0.0.1-SNAPSHOT.jar springboot-docker-image.jar
ENTRYPOINT ["java", "-jar", "springboot-docker-image.jar"]