package com.maven.app.maventhirdapp;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @RequestMapping
    public String helloWorld () {
        return "Hello World from Spring Boot - Commit Test - NEW";
    }

    @RequestMapping ("/goodbye")
    public String goodbye () {
        return "Kamal wishes you a beautiful day";
    }
}
