package com.maven.app.maventhirdapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MavenThirdAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MavenThirdAppApplication.class, args);
	}

}
